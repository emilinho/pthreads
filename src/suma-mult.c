/*
 * suma-mult.c
 * Ejemplo en C del problema de concurrencia de suma y multiplicacion
 * Andres Hernandez - tonejito
 *
 * Adaptado del codigo en python presentado en las laminas de Gunnar Wolf [1]
 * [1] http://gwolf.sistop.org/laminas/06-primitivas-sincronizacion.pdf (p4)
 *
 * Se recomienda ejecutar varias veces en secuencia para ver la disparidad
 * de los resultados causada por la condicion de carrera en la variable
 * global x:
 *
 *	% seq 0 9 | xargs -n 1 ./suma-mult
 */

#include <stdio.h>
#include <pthread.h>

/* Numero de hilos a crear por ciclo */
#define CICLOS	10
#define HILOS	2
pthread_t hilos[HILOS];

/* Resultado de las operaciones aritmeticas
 * Esta variable es donde se causa la condicion de carrera
 */
int x = 0;

/* Crea un hilo con la tarea especificada */
int hilo(pthread_t *thread, void *(*tarea) (void *));

/* Operaciones a realizar en los hilos */
void *suma(void *threadid);
void *mult(void *threadid);

/* Funcion principal */
int main(void)
{
    int n;
    /* Crea los hilos, asigna la tarea e imprime el resultado parcial */
    for (n=0; n<CICLOS; n++)
    {
        hilo(&hilos[0], suma);
        hilo(&hilos[1], mult);
        printf("%d\t", x);
    }
    /* Imprime el resultado final */
    printf("\t%d\n", x);
    pthread_exit(NULL);
}

/* Crea un hilo con la tarea especificada */
int hilo(pthread_t *thread, void *(*tarea) (void *))
{
    int rc;
    rc = pthread_create(thread, NULL, tarea, (void *) NULL);
    if(rc)
        fprintf(stderr, "ERROR, pthread_create() regreso:\t%d\n", rc);
    return rc;
}

/* Suma 2 a la variable global */
void *suma(void *threadid)
{
    putchar('+');
    x=x+2;
    pthread_exit(NULL);
}

/* Multiplica a la variable global por 3 */
void *mult(void *threadid)
{
    putchar('*');
    x=x*3;
    pthread_exit(NULL);
}

