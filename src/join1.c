/* http://www.yolinux.com/TUTORIALS/LinuxTutorialPosixThreads.html */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define NTHREADS 1000

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
int counter = 0;

void *thread_function(void *);

int main(void)
{
    pthread_t thread_id[NTHREADS];
    long i;
    int j;

    /* Crea NTHREADS hilos con la función de trabajo thread_function */
    for(i=0; i<NTHREADS; i++)
    {
        pthread_create(&thread_id[i], NULL, thread_function, (void *) i);
    }

    /* Espera a que los NTHREADS hilos terminen */
    for(j=0; j < NTHREADS; j++)
    {
        pthread_join(thread_id[j], NULL);
    }

    /* Imprime el resultado final DESPUÉS de que todos los hilos hayan terminado */
    printf("Final counter value: %d\n", counter);
    return EXIT_SUCCESS;
}

/* Thread work function */
void *thread_function(void *arg)
{
    printf("Thread number %ld (%p)\n", (long) arg, (void *) pthread_self());
    /* Perform operation using mutex */
    pthread_mutex_lock(&mutex1);
    putchar_unlocked('+');
    putchar_unlocked('\n');
    counter++;
    pthread_mutex_unlock(&mutex1);
    pthread_exit(NULL);
}
