#!/usr/bin/env bash

cat << EOF

################################################################################
Muestra todos los límites actuales para el usuario

$ ulimit -a
EOF

ulimit -a | grep --color=auto -B 13 -A 2 'max user processes'

cat << EOF

################################################################################
Muestra el límite de procesos para el usuario

$ ulimit -Hu
EOF

ulimit -Hu
