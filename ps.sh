#!/usr/bin/env bash

cat << EOF

################################################################################
Muestra las propiedades de los hilos con LWP (id) y NLWP (num de hilos)

$ ps -Lf
EOF

ps -Lf

cat << EOF

################################################################################
Muestra los hilos actuales y su id (tid)

$ ps -T
EOF

ps -T

cat << EOF

################################################################################
Muestra los procesos y sus hilos

$ ps -Lm
EOF

ps -Lm

